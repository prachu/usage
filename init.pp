node "put result of command hostname here" {

  $packages = ['git', 'htop', 'jq', 'meld', 'net-tools', 'python-pip', 'screen', 'terminator', 'vim']
  $terraform_version = 'terraform_0.11.7_linux_amd64.zip'
  $terraform_url ="https://releases.hashicorp.com/terraform/0.11.7/${terraform_version}"
#  $visual_code_key_url = "https://packages.microsoft.com/keys/microsoft.asc"
  $docker_pre_packages = ['apt-transport-https', 'ca-certificates', 'curl', 'software-properties-common']
  
  exec {'apt-get-update':
    command => '/usr/bin/apt-get update',
  }

  package { $packages:
    ensure => 'installed'
  }

  exec {'get-terraform':
    command => "/usr/bin/wget -P /tmp/ $terraform_url",
    unless  => "/usr/bin/test -f /usr/bin/terraform"
  }

  exec {'unzip-terrafor':
    command => "/usr/bin/unzip /tmp/$terraform_version -d /tmp",
    require => Exec['get-terraform'],
    unless  => "/usr/bin/test -f /usr/bin/terraform"
  } 

  exec {'move-terraform':
    command => "/bin/mv /tmp/terraform /usr/bin",
    unless  => "/usr/bin/test -f /usr/bin/terraform"
  }

#  exec {'get-microsoft-key':
#    command => "/usr/bin/wget -O /tmp/microsoft.asc $visual_code_key_url",
#    unless  => "/usr/bin/test -f /usr/bin/code"
#  }
#
#  exec {'dearmor-microsoft-key':
#    command => "/usr/bin/gpg --dearmor /tmp/microsoft.asc && mv /tmp/microsoft.asc.gpg /etc/apt/trusted.gpg.d/microsoft.gpg",
#    require => Exec['get-microsoft-key'],
#    unless  => "/usr/bin/test -f /usr/bin/code"
#  }
#
#  exec {'add-visual-code-repository':
#    command => '/bin/echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list',
#    require => Exec['dearmor-microsoft-key'],
#    notify => Exec['apt-get-update'],
#    unless  => "/usr/bin/test -f /usr/bin/code"
#  }
#
#  package {'code':
#    ensure => 'installed',
#    require => Exec['add-visual-code-repository'],
#  }

  package { $docker_pre_packages:
    ensure => 'installed',
    require => Exec['apt-get-update'],
  }

  exec {'get-docker-key':
    command => '/usr/bin/wget -O /tmp/docker-key.gpg https://download.docker.com/linux/ubuntu/gpg',
#    require => Package['$docker_pre_packages'],
    unless  => "/usr/bin/test -f /usr/bin/docker"
  }

  exec {'add-docker-key':
    command => '/usr/bin/apt-key add /tmp/docker-key.gpg',
    require => Exec['get-docker-key'],
    unless  => "/usr/bin/test -f /usr/bin/docker"
  }

  exec {'add-docker-repo':
    command => '/usr/bin/add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"',
    require => Exec['add-docker-key'],
    unless  => "/usr/bin/test -f /usr/bin/docker"
  }

  package {'docker-ce':
    ensure => 'installed',
    require => Exec['add-docker-repo']
  }

  exec {'get-packer':
    command => '/usr/bin/wget -O /tmp/packer.zip https://releases.hashicorp.com/packer/1.2.5/packer_1.2.5_linux_amd64.zip',
    unless  => '/usr/bin/test -f /usr/bin/packer'
  }

  exec {'unzip-packer':
    command => '/usr/bin/unzip /tmp/packer.zip -d /usr/bin/',
    require => Exec['get-packer'],
    unless  => '/usr/bin/test -f /usr/bin/packer'
  }

  exec {'add-notepadqq-repo':
    command => '/usr/bin/add-apt-repository ppa:notepadqq-team/notepadqq',
    notify  => Exec['apt-get-update'],
    unless  => '/usr/bin/test -f /usr/bin/notepadqq'
  }

  package {'notepadqq':
    ensure => 'installed',
    require => Exec['add-notepadqq-repo']
  }

  exec {'add-spotify-key':
    command => '/usr/bin/apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 931FF8E79F0876134EDDBDCCA87FF9DF48BF1C90',
    unless  => '/usr/bin/test -f /etc/apt/sources.list.d/spotify.list'
  }

  exec {'add-spotify-repo':
    command => '/bin/echo deb http://repository.spotify.com stable non-free > /etc/apt/sources.list.d/spotify.list',
    require => Exec['add-spotify-key'],
    notify  => Exec['apt-get-update'],
    unless  => '/usr/bin/test -f /etc/apt/sources.list.d/spotify.list'
  }

  package {'spotify-client':
    ensure => 'installed'
  }

  exec {'get-intellij':
    command => '/usr/bin/wget -O /tmp/intellij.tar.gz https://download.jetbrains.com/idea/ideaIC-2018.2.5.tar.gz',
    unless => '/usr/bin/test -d /opt/IC-182.4892.20'
  }

  exec {'extract-intellij':
    command => '/bin/tar -xzf /tmp/intellij.tar.gz -C /opt',
    unless => '/usr/bin/test -d /opt/IC-182.4892.20'
  }
}
